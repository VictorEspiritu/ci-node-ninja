const config = require('./config');
const express = require('express');
var cors = require('cors');

const app = express();
app.use(cors());

app.use(require('./routes/persona'))

app.listen(config.app.port, () => {
    console.log('Server on port '+config.app.port);
})